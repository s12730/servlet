<%@ page import="java.util.Date" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Java 4 US</title>
</head>
<body>
 <h1 ALIGN="CENTER">FormServlet</h1>
 
<FORM ACTION="http://localhost:8080/ServletForm/GetPersonServlet/" METHOD="POST">
      
      <p> <a href=" ${count}">How many candidate is log:</a></p>
	  <p>Message: ${message}</p>
      
     <hr> 
      
    <table>
    <tr>
    		
    	<td><label for="name">Name:</label></td>
    	<td><INPUT TYPE="TEXT" NAME="name" ></td>
    	
    </tr>
   
    <tr>
   		<td><label for="surname">Surname:</label></td>
		<td><INPUT TYPE="TEXT" NAME="surname"  ></td>
    </tr>
   
    <tr>
    	<td><label for="email">E-mail:</label></td>
   		<td><INPUT TYPE="text" NAME="email"></td>
    </tr>
    
    <tr>
    	<td><label for="re_email">Repeat e-mail:</label></td>
   		<td><INPUT TYPE="text" NAME="re_email" ></td>
    </tr>
    
    <tr>
    	<td><label for="employer">Employer:</label></td>
  		<td><INPUT TYPE="TEXT" NAME="employer"></td>
    
    <tr>
    	<td><label for="ad">How do you know about the conference?</label></td>
    	<td><select name="ad" >
  				<option value="jobAd">JobAd</option>
  				<option value="studyAd">studyAd</option>
  				<option value="facebook">Facebook</option>
  				<option value="friends">Friends</option>
  				<option value="another">Another</option>
			</select></td>
    </tr>
    
    <tr>
    	<td><label for="hobby">How do you do?</label></td>
    	<td><textarea name="hobby" rows="10" cols="50"></textarea></td>
    </tr>
        
   </table>
   
    <INPUT TYPE="SUBMIT" VALUE="Wyslij">
    
</FORM>

</body>
</html>