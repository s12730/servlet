package Personsql;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Data.Person;
import Parsers.sqlPersonparsers;

	public class Personsql {
	  
		private final static String DBURL = "jdbc:mysql://localhost/form";
	    private final static String DBUSER = "root";
	    private final static String DBPASS = "";
	    private final static String DBDRIVER = "com.mysql.jdbc.Driver";
	    
	    private Connection connection;
	    private Statement statement;
	    private String query;
	    private  sqlPersonparsers SqlPersonParsers ;
	    
	Personsql() {
		SqlPersonParsers = new sqlPersonparsers();
	    }
	    
	    public void save(Person person ){
	    	
	    	query = SqlPersonParsers.createSaveQuery( person);
	    	
	    	try {
	    		
	    		Class.forName(DBDRIVER).newInstance();
	    		connection = DriverManager.getConnection(DBURL, DBUSER, DBPASS);
	    		statement = connection.createStatement();
	    		statement.executeUpdate(query);
	    		
	    	}  catch (InstantiationException | IllegalAccessException
	                | ClassNotFoundException | SQLException e) {
	          
	            e.printStackTrace();
	    }
	    } 
		

		public  int count() {
	    	
	    	int count = 0;
	    	
	    	query = Personsql.createGetCountQuery();
	    	
	    	try {
	    		Class.forName(DBDRIVER);
	    		connection = DriverManager.getConnection(DBURL, DBUSER, DBPASS);
	    		statement = connection.createStatement();
	    		ResultSet rs = statement.executeQuery(query);
	    		
	    		while(rs.next()) {
	    			count = rs.getInt(1);
	    		}
	    		
	    	} catch (SQLException | ClassNotFoundException e)
	    		{
	            	e.printStackTrace();
	    		}
	    	
	    	return count;
	    

		}

		private static String createGetCountQuery() {
			
			return DBDRIVER;
		}

		
		
	}

