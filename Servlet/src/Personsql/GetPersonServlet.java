package Personsql;



import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Data.Person;
import Parsers.PersonParsers;
	


	@WebServlet("/GetPersonServlet")

	public class GetPersonServlet extends HttpServlet {
		
		
		private static final long serialVersionUID = -8907720122415394771L;
		
		private PersonParsers personParsers;
		private Personsql personsql;
		private HttpSession session;
		private String message = "Witaj!";
		
		public GetPersonServlet() {
	       
			setPersonParsers(new PersonParsers());
	     
			personsql = new Personsql();
	    }
		
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
			if(PersonParsers.count() >= 5)
			{
				message = "Niestety brak wolnych miejsc!";
			}
			request.setAttribute("count", personsql.count());
	        request.setAttribute("message", message);
	        request.getRequestDispatcher("ServletForm.jsp").forward(request, response);		
		}

		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			
			if(session == null) {
				if(personsql.count() < 5) {
					Person person = PersonParsers.parsersPersonFromRequest(request);
					if(person.getEmail().equals(person.getReEmail())) {
						session = request.getSession(true);
						PersonParsers.save(person);
						message = "Zapisano kandydata.";
					} else message = "E-mail'e nie zgdzaja sie ze soba!";			
				} else
			message = "Niestety brak wolnych miejsc!";
			} else message = "Juz zarejestrowano!";
		
			doGet(request, response);

		}

		public Personsql getPersonsql() {
			return personsql;
		}

		public void setPersonsql(Personsql personsql) {
			this.personsql = personsql;
		}

		public PersonParsers getPersonParser() {
			return personParsers;
		}

		public void setPersonParser(PersonParsers personParser) {
			this.personParsers = personParser;
		}

		public PersonParsers getPersonParsers() {
			return personParsers;
		}

		public void setPersonParsers(PersonParsers personParsers) {
			this.personParsers = personParsers;
		}

	}

